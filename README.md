# [`coredns`](https://git.dotya.ml/dotya.ml/coredns)

this repo contains the configuration files for [CoreDNS](https://coredns.io/)
set up as a [DNS over TLS (DoT)](https://en.wikipedia.org/wiki/DNS_over_TLS)
and [DNS over HTTPS (DoH)](https://en.wikipedia.org/wiki/DNS_over_HTTPS)
forwarding resolver that relies on a locally running
[`dnscrypt-proxy`](https://github.com/DNSCrypt/dnscrypt-proxy) instance for
resolution of any and all queries.

CoreDNS is configured to run under an unprivileged user (see
`coredns.service`), which doesn't by default have access to `/etc/letsencrypt`.
the TLS certs therefore need to be supplied to CoreDNS using another way - see
the `copycerts_coredns.{path,service,timer}` units.

## how to use this - tl;dr

DoT @`dns.dotya.ml`

DoH @`https://dns.dotya.ml/dns-query`

DoH alt port @`https://dns.dotya.ml:4053/dns-query`

## how to use this - the long version

the base domain here is `dns.dotya.ml`.

### DoT

simply configure the base domain directly as the standard port (`:853/tcp`) is
used.

### DoH

append `/dns-query` to the base domain and optionally prefix it with
`https://`, i.e. configure `dns.dotya.ml/dns-query` or
`https://dns.dotya.ml/dns-query` as the DoH server (in e.g.
[Firefox](https://mzl.la/3PeuP0z)).

it is worth noting that the DoH server natively listens on `:4053`, not `:443`.
however, it's *additionally* proxied by `nginx` (that hogs all
`:443`/`[::]:443` on the host) so that it can be found on the standard HTTPS
port and blends in better.

i.e. while both configurations can be used *equally* as well, one uses a
non-standard port but does not depend on `nginx` running, and the other *does*
use the standard DoH port but *could* become unavailable in the event of
`nginx` crashing for some reason. in summary, pick your set of potential
drawbacks.

## TO DO
- [ ] automated deployment (preferably using `ansible` + `drone`)

## LICENSE
WTFPLv2, see [LICENSE](LICENSE) for details
